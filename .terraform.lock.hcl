# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version     = "3.1.0"
  constraints = "~> 3.1.0"
  hashes = [
    "h1:T61KxUK1AZYguIST2a0x5Fl3NuhQMOja3Pji4Um4pQo=",
  ]
}
