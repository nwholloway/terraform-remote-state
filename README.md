# Terraform Remote State

This Terraform configuration is used to create an S3 bucket to use as
a backend to store state for other Terraform configurations.

As the state is local, and does not contain any sensitive details,
the state file is checked in.

The S3 bucket is configured as follows:

* Lifecycle to prevent accidental deletion
* Objects have versions retained for 14 days
* Objects use encryption at rest
* Objects are accessed using secure transport
* Objects can not be made public

## Creating

To create the S3 bucket, specify suitable values for `aws_region` and
`bucket_name`.

```
terraform apply --var aws_region=eu-west-1 --var bucket_name=terraform-state--demo
```

## Using

Where you want to use the S3 bucket for remote state, add the following
snippet to the Terraform configuration:

```
terraform {
  backend "s3" {
    encrypt = true
    bucket  = "terraform-state--demo"
    region  = "eu-west-1"
    key     = "sample/terraform.tfstate"
  }
}
```
